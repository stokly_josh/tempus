import * as request from "request"

enum ExpectedVersion {
	NewStream = -1,
	Any = -2,
	ShouldExist = -4
}

class NewEvent {
	public readonly id: string // unique identifier for a new event
	public readonly type: string // a label for the type of event
	public readonly data: object // the json data contained within the event
	public readonly metadata: object // the json metadata contained within the event
}

class ResolvedEvent {
	public readonly title: string
	public readonly id: string
	public readonly streamId: string
	public readonly type: string
	public readonly number: number
	public readonly data: object
	public readonly metadata: object
}

class EventEntry {
	public readonly title: string
	public readonly id: string
	public readonly type: string
	
	private _link: string
	
	public constructor(title: string, id: string, type: string, link: string) {
		this.title = title
		this.id = id
		this.type = type
		this._link = link
	}
	
//	public fetch(): ResolvedEvent {
//		
//	}
}

class StreamPage {
	public readonly title: string
	public readonly id: string
	public readonly isHead: boolean
	
	private _eTag: string
	private _entries: EventEntry[]
	private _selfLink: string
	private _firstLink: string
	private _lastLink: string
	private _nextLink: string
	private _previousLink: string | undefined

	public constructor(title: string, id: string, isHead: boolean, eTag: string, entries: EventEntry[], selfLink: string, firstLink: string, lastLink: string, nextLink: string, previousLink?: string) {
		this.title = title
		this.id = id
		this.isHead = isHead
		this._eTag = eTag
		this._entries = entries
		this._selfLink = selfLink
		this._firstLink = firstLink
		this._lastLink = lastLink
		this._nextLink = nextLink
		this._previousLink = previousLink
	}

//	public first(): StreamPage {
//		
//	}
	
//	public last(): StreamPage {
//		
//	}
	
//	public next(): StreamPage {
//		
//	}
	
//	public previous(): StreamPage {
//		// may not be available, in which case poll should be used
//	}
	
//	public poll(): StreamPage {
//		// can only be used if no previous link is available
//	}

//	public events(): ResolvedEvent[] {
//		
//	}
}

class Credentials {
	public readonly username: string
	public readonly password: string
	
	public constructor(username: string, password: string) {
		this.username = username
		this.password = password
	}
}

class Result {
	public readonly error: boolean
	public readonly message: string
}

class EventStore {
	
	private _host: string
	private _port: string
	
	public constructor(host: string, port: string) {
		this._host = host
		this._port = port
	}
	
//	public writeEventsToStream(stream: string, events: NewEvent[], expectedVersion: number = ExpectedVersion.NewStream, credentials?: Credentials): Promise<Result> {
//		
//	}
	
	public readHeadOfStream(stream: string, credentials?: Credentials): Promise<StreamPage> {
		return new Promise((resolve, reject) => {
			let uri: string = this._buildStreamUri(stream)
			let options: request.CoreOptions = {
				headers: {
					"Accept": "application/vnd.eventstore.atom+json"
				}
			}
			if (credentials) {
				options.auth = {
					user: credentials.username,
					pass: credentials.password
				}
			}
			
			request.get(uri, options,
			(error, response, body) => {
				if (error) { return reject(error) }

				let data: AtomPage = JSON.parse(body)
				
				// build entries
				let entries: EventEntry[] = []
				data.entries.forEach((entry: AtomEntry) => {
					let alternateLink: AtomLink | undefined = entry.links.find((value: AtomLink) => {
						if (value.relation == "alternate") { return true }
						return false
					})
					if ( alternateLink == undefined ) { return reject("malformed atom feed returned from eventstore") }
					
					entries.push(new EventEntry(
						entry.title,
						entry.id,
						entry.summary,
						alternateLink.uri
					))
				})
				
				// build links
				
				let firstLink: AtomLink | undefined = data.links.find((value: AtomLink) => {
					if (value.relation == "first") { return true }
					return false
				})
				if ( firstLink == undefined ) { return reject("malformed atom feed returned from eventstore") }
				
				resolve(new StreamPage(
					data.title,
					data.id,
					data.headOfStream,
					<string>response.headers.etag,
					entries,
					uri, // self
					firstLink.uri, // first
					firstLink.uri, // last
					firstLink.uri, // next
					firstLink.uri // previous
				))
			})
		})
	}
	
//	public deleteStream(stream: string, hardDelete: boolean = false, credentials?: Credentials): Promise<Result> {
//		
//	}
	
	private _buildStreamUri(stream: string): string {
		return this._host + ":" + this._port + "/streams/" + stream
	}
	
}

export { EventStore, ExpectedVersion, Credentials, StreamPage, EventEntry, ResolvedEvent, NewEvent }

interface AtomLink {
	uri: string
	relation: string
}

interface AtomEntry {
	title: string
	id: string
	updated: string
	author: {
		name: string
	}
	summary: string
	links: AtomLink[]
}

interface AtomPage {
	title: string
	id: string
	updated: string
	author: {
		name: string
	}
	headOfStream: boolean
	links: AtomLink[],
	entries: AtomEntry[]
}